# User agent parser

<!-- badges: start -->

[![GitLab CI Build Status](https://gitlab.com/artemklevtsov/nim-uap/badges/master/pipeline.svg)](https://gitlab.com/artemklevtsov/nim-uap/pipelines)
[![coverage report](https://gitlab.com/artemklevtsov/nim-uap/badges/master/coverage.svg)](https://artemklevtsov.gitlab.io/nim-uap/coverage)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

<!-- badges: end -->

Nim implementation of user-agent parser. Based on the regexes provided by the [uap-core](https://github.com/ua-parser/uap-core) project.

## installation

```bash
nimble install https://gitlab.com/artemklevtsov/nim-uap
```

## Usage

### Cli

```bash
TEST_CASES_URL="https://raw.githubusercontent.com/ua-parser/uap-core/master/tests/test_ua.yaml"
# yq link: https://github.com/stedolan/jq/releases
curl -sS "${TEST_CASES_URL}" | yq eval '.test_cases | .[].user_agent_string' > /tmp/ua.txt

# install package
export PATH=$PATH:$HOME/.nimble/bin/
nimble install -y https://gitlab.com/artemklevtsov/nim-uap
# getting help
uap --help
# works with stdin and stdout by default
head /tmp/ua.txt | uap --format json --json-pretty
# writes to csv
uap --input /tmp/ua.txt --output /tmp/ua.parsed.csv --format csv
uap --input /tmp/ua.txt --output /tmp/ua.parsed.tsv --format csv -s '\t'
# writes to json
uap --input /tmp/ua.txt --output /tmp/ua.parsed.json --format json
```

### Library

```nim
import uap

const s = "Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/7534.48.3"
# parse user agent
let ua = parseUserAgent(s)
echo($ua.browser)            # Mobile Safari 5.1.0
echo($ua.os)                 # iOS 5.1.1
echo($ua.device)             # iPhone
echo(ua.browser.version)     # 5.1.0
echo(ua.os.version)          # 5.1.1
echo($ua.device.isSpider)    # false
echo($ua.isSpider)           # false
# detect device type
let dt = parseDeviceType(s)
echo($dt)                    # Mobile
# parse browser
echo($parseBrowser(s))       # Mobile Safari 5.1.0
# parse os
echo($parseOS(s))            # iOS 5.1.1
# parse device
echo($parseDevice(s))        # iPhone
```
