import std/unittest
import std/json
import uap

include skipoutput

suite "Test browser toJson":

  test "Test empty browser":
    let a = initBrowserAgent()
    check %a == %*{"family": "Other"}

  test "Test OS family and version":
    let a = initBrowserAgent(family = "Mobile Safari", major = "5", minor = "1")
    check %a == %*{"family": "Mobile Safari", "major": "5", "minor": "1"}

  test "Test OS family and full version":
    let a = initBrowserAgent(family = "Mobile Safari", major = "5", minor = "1", patch = "1")
    check %a == %*{"family": "Mobile Safari", "major": "5", "minor": "1", "patch": "1"}



suite "OsAgent toJson":

  test "Test empty OS":
    let a = initOsAgent()
    check %a == %*{"family": "Other"}

  test "Test OS family and version":
    let a = initOsAgent(family = "iOS", major = "5", minor = "1")
    check %a == %*{"family": "iOS", "major": "5", "minor": "1"}

  test "Test OS family and full version":
    let a = initOsAgent(family = "iOS", major = "5", minor = "1", patch = "1", patchMinor = "9999")
    check %a == %*{"family": "iOS", "major": "5", "minor": "1", "patch": "1", "patchMinor": "9999"}


suite "DeviceAgent toJson":

  test "Test empty device":
    let a = initDeviceAgent()
    check %a == %*{"family": "Other", "spider": false}

  test "Test device family":
    let a = initDeviceAgent(family = "iPhone")
    check %a == %*{"family": "iPhone", "spider": false}

  test "Test device family and brand":
    let a = initDeviceAgent(family = "iPhone", brand = "Apple")
    check %a == %*{"family": "iPhone", "brand": "Apple", "spider": false}

  test "Test device family and brand and model":
    let a = initDeviceAgent(family = "iPhone", brand = "Apple", model = "Test")
    check %a == %*{"family": "iPhone", "brand": "Apple", "model": "Test", "spider": false}


suite "UserAgent toJson":

  test "Test empty agent":
    let a = UserAgent(browser: initBrowserAgent(), os: initOsAgent(), device: initDeviceAgent())
    check %a == %*{"browser": {"family": "Other"}, "os": {"family": "Other"}, "device": {"family": "Other", "spider": false}}
