import std/os
import std/unittest
import uap

include skipoutput

const testFilesDir = "tests" / "files"

# parse device type
suite "Parse device type":

  test "Test parse device type":
    check parseDeviceType("") == DeviceType.Unknown

  test "Test mobile":
    let s1 = "Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X) " &
             "AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 " &
             "Mobile/9B206 Safari/7534.48.3"
    check parseDeviceType(s1) == DeviceType.Mobile
    let s2 = "Mozilla/5.0 (Linux; Android 8.0.0; SAMSUNG SM-J330FN Build/R16NW) " &
             "AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/7.2 " &
             "Chrome/11.1.1111.111 Mobile Safari/537.36"
    check parseDeviceType(s2) == DeviceType.Mobile

  test "Test tablet":
    let s1 = "Mozilla/5.0 (Linux; U; en-us; KFTT Build/IML74K) " &
             "AppleWebKit/535.19 (KHTML, like Gecko) Silk/2.0 " &
             "Safari/535.19 Silk-Accelerated=false"
    check parseDeviceType(s1) == DeviceType.Tablet
    let s2 = "Mozilla/5.0 (Linux; Android 9; SHT-AL09 Build/HUAWEISHT-AL09; wv) " &
             "AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 " &
             "Chrome/79.0.3945.136 Safari/537.36"
    check parseDeviceType(s2) == DeviceType.Tablet

  test "Test desktop":
    let s1 = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.4 (KHTML, like " &
             "Gecko) Chrome/98 Safari/537.4 (StatusCake)"
    check parseDeviceType(s1) == DeviceType.Desktop

suite "Parse device type test cases":

  for s in lines(testFilesDir / "test_device_type_mobile.txt"):
    if s.len == 0:
      continue
    test "Test parseDeviceType: " & $s:
      let ans = {DeviceType.Mobile, DeviceType.Tablet}
      check parseDeviceType(s) in ans
