import std/os
import std/json
import std/macros
import std/unittest
import uap

include skipoutput

const testFilesDir = "tests" / "files"

type
  TestString = tuple
    user_agent_string: string
  TestInput = object
    test_cases: seq[TestString]
  TestExpected[T] = object
    test_cases: seq[T]

macro getProcName(someProc: typed): untyped =
  ## Get proc name.
  let impl = getImpl(someProc)
  expectKind(impl, nnkProcDef)
  toStrLit(name(impl))

macro getProcRetType(someProc: typed): untyped =
  ## Get proc return type.
  let impl = getImpl(someProc)
  expectKind(impl, nnkProcDef)
  params(impl)[0]

template testUaParser(path: string, fun: untyped): untyped =
  ## Contruct test cases by given test file.
  type typ = getProcRetType(fun)
  let procName = getProcName(fun)
  let cnt = readFile(path).parseJson()
  let strings = cnt.to(TestInput).test_cases
  let agents = cnt.to(TestExpected[typ]).test_cases
  assert strings.len == agents.len
  for i in 0 ..< strings.len:
    test "Test " & procName & ": " & strings[i].user_agent_string:
      check fun(strings[i][0]) == agents[i]

# parse browser
suite "Parse browser test cases":
  testUaParser(testFilesDir / "test_ua.json", parseBrowser)
  testUaParser(testFilesDir / "firefox_user_agent_strings.json", parseBrowser)
  testUaParser(testFilesDir / "pgts_browser_list.json", parseBrowser)
  testUaParser(testFilesDir / "opera_mini_user_agent_strings.json", parseBrowser)
  testUaParser(testFilesDir / "podcasting_user_agent_strings.json", parseBrowser)

# parse os
suite "Parse OS test cases":
  testUaParser(testFilesDir / "test_os.json", parseOs)
  testUaParser(testFilesDir / "additional_os_tests.json", parseOs)

# parse device
suite "Parse device test cases":
  testUaParser(testFilesDir / "test_device.json", parseDevice)
