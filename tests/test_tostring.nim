import std/unittest
import uap

include skipoutput

suite "BrowserAgent toString":

  test "Test empty browser":
    let a = initBrowserAgent()
    check $a == "Other 0.0.0"

  test "Test OS family and version":
    let a = initBrowserAgent(family = "Mobile Safari", major = "5", minor = "1")
    check $a == "Mobile Safari 5.1.0"

  test "Test OS family and full version":
    let a = initBrowserAgent(family = "Mobile Safari", major = "5", minor = "1", patch = "1")
    check $a == "Mobile Safari 5.1.1"


suite "OsAgent toString":

  test "Test empty OS":
    let a = initOsAgent()
    check $a == "Other 0.0.0"

  test "Test OS family and version":
    let a = initOsAgent(family = "iOS", major = "5", minor = "1", patch = "1")
    check $a == "iOS 5.1.1"

  test "Test OS family and full version":
    let a = initOsAgent(family = "iOS", major = "5", minor = "1", patch = "1", patchMinor = "9999")
    check $a == "iOS 5.1.1.9999"


suite "DeviceAgent toString":

  test "Test empty device":
    let a = initDeviceAgent()
    check $a == "Other"

  test "Test device family":
    let a = initDeviceAgent(family = "iPhone")
    check $a == "iPhone"

  test "Test device family and brand":
    let a = initDeviceAgent(family = "iPhone", brand = "Apple")
    check $a == "iPhone"

  test "Test device family and brand and model":
    let a = initDeviceAgent(family = "iPhone", brand = "Apple", model = "Test")
    check $a == "iPhone"


suite "UserAgent toString":

  test "Test empty agent":
    let a = UserAgent(browser: initBrowserAgent(), os: initOsAgent(), device: initDeviceAgent())
    check $a == "Other 0.0.0/Other 0.0.0"
