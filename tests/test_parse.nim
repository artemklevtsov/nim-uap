import std/unittest
import uap

include skipoutput

suite "Test parse browser":

  test "Parse browser Safari":
    let s = "Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X) " &
            "AppleWebKit/534.46 " &
            "(KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/7534.48.3"
    let a = parseBrowser(s)
    check a.family == "Mobile Safari"
    check a.major == "5"
    check a.minor == "1"
    check a.patch == ""

suite "Test parse OS":

  test "Parse OS Safari":
    let s = "Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X) " &
            "AppleWebKit/534.46 " &
            "(KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/7534.48.3"
    let a = parseOS(s)
    check a.family == "iOS"
    check a.major == "5"
    check a.minor == "1"
    check a.patch == "1"
    check a.patchMinor == ""

suite "Test parse device":

  test "Parse device Safari":
    let s = "Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X) " &
            "AppleWebKit/534.46 " &
            "(KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/7534.48.3"
    let a = parseDevice(s)
    check a.family == "iPhone"
    check a.brand == "Apple"
    check a.model == "iPhone"

  test "Detect spider":
    let s1 = "Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X) " &
             "AppleWebKit/534.46 " &
             "(KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/7534.48.3"
    check parseDevice(s1).isSpider() == false
    let s2 = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
    check parseDevice(s2).isSpider() == true
    let s3 = "ICC-Crawler/2.0 (Mozilla-compatible; ; http://kc.nict.go.jp/project1/crawl.html; zurukko1552995316;)"
    check parseDevice(s3).isSpider() == true
