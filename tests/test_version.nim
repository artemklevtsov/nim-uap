import std/unittest
import uap

include skipoutput

suite "BrowserAgent version":

  test "Test browser empty version":
    let a = initBrowserAgent()
    check a.version == "0.0.0"

  test "Test browser major version":
    let a = initBrowserAgent(major = "1")
    check a.version == "1.0.0"

  test "Test browser minor version":
    let a = initBrowserAgent(minor = "1")
    check a.version == "0.1.0"

  test "Test browser patch version":
    let a = initBrowserAgent(patch = "1")
    check a.version == "0.0.1"

  test "Test browser full version":
    let a = initBrowserAgent(major = "1", minor = "2", patch = "3")
    check a.version == "1.2.3"


suite "OsAgent version":

  test "Test OS empty version":
    let a = initBrowserAgent()
    check a.version == "0.0.0"

  test "Test OS major version":
    let a = initOsAgent(major = "1")
    check a.version == "1.0.0"

  test "Test OS minor version":
    let a = initOsAgent(minor = "1")
    check a.version == "0.1.0"

  test "Test OS patch version":
    let a = initOsAgent(patch = "1")
    check a.version == "0.0.1"

  test "Test OS patchMinor version":
    let a = initOsAgent(patchMinor = "1")
    check a.version == "0.0.0.1"

  test "Test OS full version":
    let a = initOsAgent(major = "1", minor = "2", patch = "3")
    check a.version == "1.2.3"

  test "Test OS full with patch version":
    let a = initOsAgent(major = "1", minor = "2", patch = "3", patchMinor = "4")
    check a.version == "1.2.3.4"
