import std/strutils
import std/options
import std/re
import std/json

type
  BrowserParser* = object
    regex*: string
    family_replacement*: Option[string]
    v1_replacement*: Option[string]
    v2_replacement*: Option[string]
    v3_replacement*: Option[string]

  OsParser* = object
    regex*: string
    os_replacement*: Option[string]
    os_v1_replacement*: Option[string]
    os_v2_replacement*: Option[string]
    os_v3_replacement*: Option[string]

  DeviceParser* = object
    regex*: string
    regex_flag*: Option[string]
    device_replacement*: Option[string]
    brand_replacement*: Option[string]
    model_replacement*: Option[string]

  UserAgentParsers* = object
    user_agent_parsers*: seq[BrowserParser]
    os_parsers*: seq[OsParser]
    device_parsers*: seq[DeviceParser]

  ParserMatches* = array[6, string]

  ParserResult*[T] = tuple
    status: bool
    parser: T
    matches: ParserMatches

proc newRegex[T](parser: T): Regex =
  ## Init user agent parser with compiled regex.
  var reFlags = {reStudy}
  when parser is DeviceParser:
    if parser.regex_flag.get("") == "i":
      reFlags.incl(reIgnoreCase)
  result = re(parser.regex, reFlags)

proc initRegexes[T](r: var seq[Regex], p: seq[T]) =
  ## Init regexes from the parsers.
  # init regexes (at runtime)
  if unlikely(r.len == 0):
    r.setLen(p.len)
    for i in 0 ..< p.len:
      r[i] = newRegex(p[i])

proc matchParser*[T](s: string, regexes: var seq[Regex], parsers: seq[T]): ParserResult[T] =
  ## Find matched parser.
  result.status = false
  if s.len == 0:
    return
  initRegexes(regexes, parsers)
  for i in 0 ..< regexes.len:
    var m: ParserMatches
    if s.find(regexes[i], m) != -1:
      return (true, parsers[i], m)

# load all parsers (at compile time)
let uaParsers* {.compileTime.} = staticRead("regexes.json").parseJson().to(UserAgentParsers)
