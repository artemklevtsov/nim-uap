##[
  A Nim user-agent string parser based on `ua-parser/uap-core<https://github.com/ua-parser/uap-core>`_.
  It extracts browser, OS and device information.

  Examples
  ========

  .. code-block:: nim
    import uap

    const s = "Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/7534.48.3"
    # parse user agent
    let ua = parseUserAgent(s)
    echo($ua.browser)            # Mobile Safari 5.1.0
    echo($ua.os)                 # iOS 5.1.1
    echo($ua.device)             # iPhone
    echo(ua.browser.version)     # 5.1.0
    echo(ua.os.version)          # 5.1.1
    echo($ua.device.isSpider)    # false
    echo($ua.isSpider)           # false
    # detect device type
    let dt = parseDeviceType(s)
    echo($dt)                    # Mobile
    # parse browser
    echo($parseBrowser(s))       # Mobile Safari 5.1.0
    # parse os
    echo($parseOS(s))            # iOS 5.1.1
    # parse device
    echo($parseDevice(s))        # iPhone
]##


import std/strutils
import std/options
import std/re
import std/json
import parsers

type
  BrowserAgent* = object
    ## Brouser user agent type.
    family*: string
    major*: string
    minor*: string
    patch*: string

  OsAgent* = object
    ## OS user agent type.
    family*: string
    major*: string
    minor*: string
    patch*: string
    patch_minor*: string

  DeviceAgent* = object
    ## Device user agent type.
    family*: string
    model*: string
    brand*: string

  UserAgent* = object
    ## User Agent type.
    browser*: BrowserAgent
    os*: OsAgent
    device*: DeviceAgent

  DeviceType* {.pure.} = enum
    ## Supported device types.
    Unknown, Desktop, Mobile, Tablet

var browserRegexes {.threadvar.}: seq[Regex]
var osRegexes {.threadvar.}: seq[Regex]
var deviceRegexes {.threadvar.}: seq[Regex]


# Browser

proc initBrowserAgent*(family: string = "Other", major, minor, patch: string = ""): BrowserAgent =
  ## Init BrowserAgent with parser and macthes.
  result.family = family
  result.major  = major
  result.minor  = minor
  result.patch  = patch

proc initBrowserAgent(r: ParserResult): BrowserAgent =
  ## Init BrowserAgent with parser and macthes.
  if not r.status:
    result.family = "Other"
  else:
    proc cb(x: string): string = x.format(r.matches)
    result.family = r.parser.family_replacement.map(cb).get(r.matches[0])
    result.major  = r.parser.v1_replacement.get(r.matches[1])
    result.minor  = r.parser.v2_replacement.get(r.matches[2])
    result.patch  = r.parser.v3_replacement.get(r.matches[3])

proc version*(a: BrowserAgent): string =
  ## User agent version string.
  ##
  runnableExamples:
    doAssert initBrowserAgent("Safari", "13", "1", "1").version == "13.1.1"
    doAssert initBrowserAgent("Mobile Safari", "5", "1").version == "5.1.0"

  result.add if a.major.len > 0: a.major else: "0"
  result.add "."
  result.add if a.minor.len > 0: a.minor else: "0"
  result.add "."
  result.add if a.patch.len > 0: a.patch else: "0"

proc `$`*(a: BrowserAgent): string =
  ## Converts Agent to string on one line.
  ##
  runnableExamples:
    doAssert $initBrowserAgent("Safari", "13", "1", "1") == "Safari 13.1.1"

  a.family & " " & a.version

proc `%`*(a: BrowserAgent): JsonNode =
  ## Converts Agent to JSON.
  result = newJObject()
  result["family"] = %a.family
  if a.major.len > 0: result["major"] = %a.major
  if a.minor.len > 0: result["minor"] = %a.minor
  if a.patch.len > 0: result["patch"] = %a.patch

proc parseBrowser*(s: string): BrowserAgent =
  ## Returns information of the family type of the User-Agent.
  ##
  runnableExamples:
    const s = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4)" &
              "AppleWebKit/605.1.15 (KHTML, like Gecko)" &
              "Version/13.1.1 Safari/605.1.15"
    let b = parseBrowser(s)
    doAssert b.family == "Safari"
    doAssert b.major == "13"
    doAssert b.minor == "1"
    doAssert b.patch == "1"

  const browserParsers = uaParsers.user_agent_parsers
  initBrowserAgent(s.matchParser(browserRegexes, browserParsers))


# OS

proc initOsAgent*(family: string = "Other", major, minor, patch, patchMinor: string = ""): OsAgent =
  ## Init OsAgent with parser and macthes.
  result.family = family
  result.major  = major
  result.minor  = minor
  result.patch  = patch
  result.patchMinor = patchMinor

proc initOsAgent(r: ParserResult): OsAgent =
  ## Init OsAgent with parser and macthes.
  if not r.status:
    result.family = "Other"
  else:
    proc cb(x: string): string = x.format(r.matches)
    result.family = r.parser.os_replacement.map(cb).get(r.matches[0])
    result.major  = r.parser.os_v1_replacement.map(cb).get(r.matches[1])
    result.minor  = r.parser.os_v2_replacement.map(cb).get(r.matches[2])
    result.patch  = r.parser.os_v3_replacement.map(cb).get(r.matches[3])
    result.patchMinor = r.matches[4]

proc version*(a: OsAgent): string =
  ## User agent version string.
  ##
  runnableExamples:
    doAssert initOsAgent("Mac OS X", "10", "15", "4").version == "10.15.4"

  result.add if a.major.len > 0: a.major else: "0"
  result.add "."
  result.add if a.minor.len > 0: a.minor else: "0"
  result.add "."
  result.add if a.patch.len > 0: a.patch else: "0"
  result.add if a.patchMinor.len > 0: "." & a.patchMinor else: ""

proc `$`*(a: OsAgent): string =
  ## Converts Agent to string on one line.
  runnableExamples:
    doAssert $initOsAgent("Mac OS X", "10", "15", "4") == "Mac OS X 10.15.4"

  a.family & " " & a.version

proc `%`*(a: OsAgent): JsonNode =
  ## Converts Agent to JSON.
  result = newJObject()
  result["family"] = %a.family
  if a.major.len > 0: result["major"] = %a.major
  if a.minor.len > 0: result["minor"] = %a.minor
  if a.patch.len > 0: result["patch"] = %a.patch
  if a.patchMinor.len > 0: result["patchMinor"] = %a.patchMinor

proc parseOS*(s: string): OsAgent =
  ## Returns information of the os type of the Operating System (OS) the User-Agent runs.
  ##
  runnableExamples:
    const s = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4)" &
              "AppleWebKit/605.1.15 (KHTML, like Gecko)" &
              "Version/13.1.1 Safari/605.1.15"
    let o = parseOS(s)
    doAssert o.family == "Mac OS X"
    doAssert o.major == "10"
    doAssert o.minor == "15"
    doAssert o.patch == "4"

  const osParsers = uaParsers.os_parsers
  initOsAgent(s.matchParser(osRegexes, osParsers))


# Device

proc initDeviceAgent*(family: string = "Other", brand, model: string = ""): DeviceAgent =
  ## Init DeviceAgent with parser and macthes.
  result.family = family
  result.brand  = brand
  result.model  = model

proc initDeviceAgent(r: ParserResult): DeviceAgent =
  ## Init DeviceAgent with parser and macthes.
  if not r.status:
    result.family = "Other"
  else:
    proc cb(x: string): string = x.format(r.matches).strip()
    result.family = r.parser.device_replacement.map(cb).get(r.matches[0])
    result.brand  = r.parser.brand_replacement.map(cb).get("")
    result.model  = r.parser.model_replacement.map(cb).get(r.matches[0])

proc isSpider*(a: DeviceAgent): bool =
  ## Detects Device is spider.
  ##
  runnableExamples:
    doAssert parseDevice("GooglePlusBot").isSpider
    doAssert parseDevice("FacebookBot").isSpider

  a.family == "Spider"

proc `$`*(a: DeviceAgent): string =
  ## Converts Device to string on one line.
  runnableExamples:
    doAssert $initDeviceAgent("Mac", "Mac", "Apple") == "Mac"

  a.family

proc `%`*(a: DeviceAgent): JsonNode =
  ## Converts Agent to JSON.
  result = newJObject()
  result["family"] = %a.family
  if a.model.len > 0: result["model"] = %a.model
  if a.brand.len > 0: result["brand"] = %a.brand
  result["spider"] = %a.isSpider

proc parseDevice*(s: string): DeviceAgent =
  ## Returns information of the device family the User-Agent runs on
  ##
  runnableExamples:
    const s = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4)" &
              "AppleWebKit/605.1.15 (KHTML, like Gecko)" &
              "Version/13.1.1 Safari/605.1.15"
    let d = parseDevice(s)
    doAssert d.family == "Mac"
    doAssert d.model == "Mac"
    doAssert d.brand == "Apple"

  const deviceParsers = uaParsers.device_parsers
  initDeviceAgent(s.matchParser(deviceRegexes, deviceParsers))


# User Agent

proc `$`*(ua: UserAgent): string =
  ## Converts UserAgent to string on one line.
  $ua.browser & "/" & $ua.os

proc `%`*(ua: UserAgent): JsonNode =
  ## Converts Agent to JSON.
  result = newJObject()
  result["browser"] = %ua.browser
  result["os"] = %ua.os
  result["device"] = %ua.device

proc isSpider*(ua: UserAgent): bool =
  ## Detects UserAgent is spider.
  ua.device.isSpider()

proc parseUserAgent*(s: string): UserAgent =
  ## Return user agent information of the browser, OS and device.
  ##
  runnableExamples:
    const s = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4)" &
              "AppleWebKit/605.1.15 (KHTML, like Gecko)" &
              "Version/13.1.1 Safari/605.1.15"
    let u = parseUserAgent(s)
    doAssert u.browser.family == "Safari"
    doAssert u.browser.major == "13"
    doAssert u.browser.minor == "1"
    doAssert u.browser.patch == "1"
    doAssert u.os.family == "Mac OS X"
    doAssert u.os.major == "10"
    doAssert u.os.minor == "15"
    doAssert u.os.patch == "4"
    doAssert u.device.family == "Mac"
    doAssert u.device.model == "Mac"
    doAssert u.device.brand == "Apple"

  result.browser = parseBrowser(s)
  result.os = parseOS(s)
  result.device = parseDevice(s)


# Device Type

proc parseDeviceType*(s: string): DeviceType =
  ## Returns device type.
  ##
  runnableExamples:
    const s1 = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4)" &
               "AppleWebKit/605.1.15 (KHTML, like Gecko)" &
               "Version/13.1.1 Safari/605.1.15"
    const s2 = "Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X)" &
               "AppleWebKit/534.46 (KHTML, like Gecko)" &
               "Version/5.1 Mobile/9B206 Safari/7534.48.3"
    const s3 = "Mozilla/5.0 (Linux; U; en-us; KFTT Build/IML74K) " &
               "AppleWebKit/535.19 (KHTML, like Gecko) Silk/2.0 " &
               "Safari/535.19 Silk-Accelerated=false"
    doAssert $parseDeviceType(s1) == "Desktop"
    doAssert $parseDeviceType(s2) == "Mobile"
    doAssert $parseDeviceType(s3) == "Tablet"

  # https://gist.github.com/dalethedeveloper/1503252/931cc8b613aaa930ef92a4027916e6687d07feac
  const mobilePattern = [
    "Mobile",
    "iP(hone|od|ad)",
    "Android",
    "BlackBerry",
    "IEMobile",
    "Kindle",
    "NetFront",
    "Silk-Accelerated",
    "(hpw|web)OS",
    "Fennec",
    "Minimo",
    "Opera M(obi|ini)",
    "Blazer",
    "Dolfin",
    "Dolphin",
    "Skyfire",
    "Zune",
  ].join("|")

  const tabletPattern = [
    "(tablet|ipad|playbook|silk)",
    "(android.*)",
  ].join("|")

  let mobileRegex = re(mobilePattern, {reStudy})
  let tabletRegex = re(tabletPattern, {reStudy, reIgnoreCase})

  if s.len == 0:
    return DeviceType.Unknown
  var matches: array[4, string]
  if s.find(tabletRegex, matches) != -1 and matches[1].find("Mobile") == -1:
    return DeviceType.Tablet
  elif s.find(mobileRegex) != -1:
    return DeviceType.Mobile
  else:
    return DeviceType.Desktop
