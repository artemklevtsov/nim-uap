import std/terminal
import std/strutils
import std/strformat
import std/sequtils
import std/json
import ua

type
  OutputFormat* = enum
    ofText = "text",
    ofJson = "json",
    ofCsv = "csv",

  ParserMode* = enum
    pmBrowser = "browser"
    pmDevice = "device"
    pmOs = "os"
    pmSpider = "spider"


# CSV

proc csvHeader(mode: set[ParserMode]): seq[string] =
  result.add "user_agent"
  for m in mode:
    case m
    of pmBrowser:
      result.add ["family", "major", "minor", "patch"].mapIt("browser_" & it)
    of pmOs:
      result.add ["family", "major", "minor", "patch", "patchMinor"].mapIt("os_" & it)
    of pmDevice:
      result.add ["family", "model", "brand"].mapIt("device_" & it)
    of pmSpider:
      result.add "spider"

proc csvRow(line: string, mode: set[ParserMode]): seq[string] =
  result.add line
  for m in mode:
    case m
    of pmBrowser:
      let b = parseBrowser(line)
      result.add b.family
      result.add b.major
      result.add b.minor
      result.add b.patch
    of pmOs:
      let o = parseOs(line)
      result.add o.family
      result.add o.major
      result.add o.minor
      result.add o.patch
      result.add o.patchMinor
    of pmDevice:
      let d = parseDevice(line)
      result.add d.family
      result.add d.model
      result.add d.brand
    of pmSpider:
      result.add $parseDevice(line).isSpider

proc writeCsv*(input: File, output: File, mode: set[ParserMode], sep: char = ',') =
  var header = true
  for line in input.lines:
    if header:
      output.write csvHeader(mode).join($sep)
      output.write '\n'
      header = false
    output.write csvRow(line, mode).join($sep)
    output.write '\n'


# JSON

proc jsonEntry(line: string, mode: set[ParserMode]): JsonNode =
  result = newJObject()
  result["user_agent"] = %line
  for m in mode:
    case m
    of pmBrowser:
      result["browser"] = %parseBrowser(line)
    of pmOs:
      result["os"] = %parseOs(line)
    of pmDevice:
      result["device"] = %parseDevice(line)
    of pmSpider:
      result["spider"] = %(parseDevice(line).isSpider)

proc writeJson*(input: File, output: File, mode: set[ParserMode], pretty: bool = false) =
  const indentCount = 2
  var comma = false
  output.write '['
  if pretty:
    output.write '\n'
  for line in input.lines:
    if comma:
      output.write ','
      if pretty:
        output.write '\n'
    else:
      comma = true
    let j = jsonEntry(line, mode)
    output.write if pretty: j.pretty(indentCount).indent(indentCount) else: $j
  if pretty:
    output.write '\n'
  output.write ']'


# Text

proc writeText*(input: File, output: File) =
  for line in input.lines:
    output.write $parseUserAgent(line)
    output.write '\n'


# Common

template failure(msg: string) =
  echo msg & ": " & getCurrentExceptionMsg()
  return QuitFailure

proc parse*(input: string = "", output: string = "",
            mode: set[ParserMode] = {pmBrowser,pmOs,pmDevice,pmSpider},
            format: OutputFormat = ofText,
            jsonPretty: bool = false, csvSep: char = ','): int =

  var inputFile: File
  var outputFile: File

  try:
    inputFile = if input != "": open(input, fmRead) else: stdin
  except IOError:
    failure &"Can not open input file '{input}' to read"

  try:
    outputFile = if output != "": open(output, fmWrite) else: stdout
  except IOError:
    failure &"Can not open output file '{output}' to write"

  if input == "" and isatty(stdin):
    return QuitSuccess

  try:
    case format
    of ofJson:
      writeJson(inputFile, outputFile, mode, jsonPretty)
    of ofCsv:
      writeCsv(inputFile, outputFile, mode, csvSep)
    of ofText:
      writeText(inputFile, outputFile)
  except:
    failure &"Can not write output to '{output}'"

  close(inputFile)
  close(outputFile)

  return QuitSuccess
