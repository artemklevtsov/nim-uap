import uappkg/ua

export ua

when isMainModule:
  import std/strformat
  import std/json
  import cligen
  import uappkg/cli

  proc getUapVersion(): string {.compileTime.} =
    ## User agent parser regexes version.
    let j = parseJson(staticRead("../uap-core/package.json"))
    j["version"].getStr()

  const uapVersion = getUapVersion()
  ## User agent parser regexes version.

  const nimbleVersion = staticRead("../uap.nimble").fromNimble("version")
  clCfg.version = &"{nimbleVersion} (uap-core {uapVersion})"

  dispatch(
    parse,
    cmdName = "uap",
    short = {
      "version": 'V',
      "json-pretty": 'p',
      "csv-sep": 's',
    },
    help = {
      "version": "Print Version & Exit 0",
      "input": "input file (default: stdin)",
      "output": "output file (default: stdout)",
      "mode": "parser mode (default: all)",
      "format": "output format (default: text)",
      "json-pretty": "prettify output for JSON output (default: false)",
      "csv-sep": "separator for CSV output (default: ',')",
    },
    noAutoEcho = true
  )
