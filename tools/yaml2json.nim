import std/os
import std/json
import yaml

proc fixext(path: string): string =
  path.extractFilename().changeFileExt("json")

proc yaml2json(src, dst: string) =
  writeFile(dst, $(readFile(src).loadToJson()[0]))

when isMainModule:
  let cd = getCurrentDir()
  let uapDir = cd / "uap-core"
  let srcDir = cd / "src" / "uappkg"
  let testDir = cd / "tests" / "files"
  yaml2json(uapDir / "regexes.yaml", srcDir / "regexes.json")
  for f in walkFiles(uapDir / "tests" / "*.yaml"):
    yaml2json(f, testDir / fixext(f))
  for f in walkFiles(uapDir / "test_resources" / "*.yaml"):
    yaml2json(f, testDir / fixext(f))
